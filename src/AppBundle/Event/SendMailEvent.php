<?php

namespace AppBundle\Event;

use AppBundle\Entity\Contact;
use Symfony\Component\EventDispatcher\Event;

class SendMailEvent extends Event
{

	const NAME = 'mail.notification';

	/**
	 * @var Contact
	 */
	private $contact;

	public function __construct( Contact $contact ) {

		$this->contact = $contact;
	}

	/**
	 * @return Contact
	 */
	public function getContact(): Contact {
		return $this->contact;
	}



}