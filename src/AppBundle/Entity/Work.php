<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Work
 *
 * @ORM\Table(name="work")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorkRepository")
 */
class Work
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="guid")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="UUID")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="slug", type="string", length=255, unique=true)
	 */
	private $slug;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="live", type="boolean")
	 */
	private $live = false;

	/**
	 * @var
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Category", inversedBy="works")
	 * @ORM\JoinTable(name="works_categories")
	 *
	 */
	private $categories;

	/**
	 * @var
	 *
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image", mappedBy="work", cascade={"persist", "remove"})
	 *
	 */
	private $images;


	public function __construct()
	{
		$this->categories = new ArrayCollection();
		$this->images = new ArrayCollection();
	}


	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Work
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Work
	 */
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set content
	 *
	 * @param string $content
	 *
	 * @return Work
	 */
	public function setContent($content)
	{
		$this->content = $content;

		return $this;
	}

	/**
	 * Get content
	 *
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * Set slug
	 *
	 * @param string $slug
	 *
	 * @return Work
	 */
	public function setSlug($slug)
	{
		$this->slug = $slug;

		return $this;
	}

	/**
	 * Get slug
	 *
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * @param bool $live
	 */
	public function setLive( $live ) {
		$this->live = $live;
	}

	/**
	 * Add category
	 *
	 * @param \AppBundle\Entity\Category $category
	 *
	 * @return Work
	 */
	public function addCategory(\AppBundle\Entity\Category $category)
	{
		$this->categories[] = $category;

		return $this;
	}

	/**
	 * Remove category
	 *
	 * @param \AppBundle\Entity\Category $category
	 */
	public function removeCategory(\AppBundle\Entity\Category $category)
	{
		$this->categories->removeElement($category);
	}

	/**
	 * Get categories
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCategories()
	{
		return $this->categories;
	}

	/**
	 * Add image
	 *
	 * @param \AppBundle\Entity\Image $image
	 *
	 * @return Work
	 */
	public function addImage(\AppBundle\Entity\Image $image)
	{
		$this->images[] = $image;

		return $this;
	}

	/**
	 * Remove image
	 *
	 * @param \AppBundle\Entity\Image $image
	 */
	public function removeImage(\AppBundle\Entity\Image $image)
	{
		$this->images->removeElement($image);
	}

	/**
	 * Get images
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getImages()
	{
		return $this->images;
	}

	/**
	 * Get live
	 *
	 * @return boolean
	 */
	public function getLive()
	{
		return $this->live;
	}
}
