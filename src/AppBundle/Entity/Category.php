<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="guid")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="UUID")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="slug", type="string", length=255, unique=true)
	 */
	private $slug;

	/**
	 * @var
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Work", mappedBy="categories")
	 */
	private $works;


	public function __construct()
	{
		$this->works = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Category
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Category
	 */
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set slug
	 *
	 * @param string $slug
	 *
	 * @return Category
	 */
	public function setSlug($slug)
	{
		$this->slug = $slug;

		return $this;
	}

	/**
	 * Get slug
	 *
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * Add work
	 *
	 * @param \AppBundle\Entity\Work $work
	 *
	 * @return Category
	 */
	public function addWork(\AppBundle\Entity\Work $work)
	{
		$this->works[] = $work;

		return $this;
	}

	/**
	 * Remove work
	 *
	 * @param \AppBundle\Entity\Work $work
	 */
	public function removeWork(\AppBundle\Entity\Work $work)
	{
		$this->works->removeElement($work);
	}

	/**
	 * Get works
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getWorks()
	{
		return $this->works;
	}
}
