<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 */
class Image
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="guid")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="UUID")
	 */
	private $id;

	/**
	 * @ORM\Column(name="public_id", type="string")
	 */
	private $publicId;

	/**
	 */
	private $image;

	/**
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createdAt;

	/**
	 * @ORM\Column(name="url", type="string")
	 */
	private $url;

	/**
	 * @ORM\Column(name="secure_url", type="string")
	 */
	private $secureUrl;

	/**
	 * @var
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Work", inversedBy="images")
	 *
	 */
	private $work;


	public function __construct()
	{
		$this->createdAt = new \DateTime();
	}



	/**
	 * Get id
	 *
	 * @return guid
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set publicId
	 *
	 * @param string $publicId
	 *
	 * @return Image
	 */
	public function setPublicId($publicId)
	{
		$this->publicId = $publicId;

		return $this;
	}

	/**
	 * Get publicId
	 *
	 * @return string
	 */
	public function getPublicId()
	{
		return $this->publicId;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return Image
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set url
	 *
	 * @param string $url
	 *
	 * @return Image
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * Get url
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Set secureUrl
	 *
	 * @param string $secureUrl
	 *
	 * @return Image
	 */
	public function setSecureUrl($secureUrl)
	{
		$this->secureUrl = $secureUrl;

		return $this;
	}

	/**
	 * Get secureUrl
	 *
	 * @return string
	 */
	public function getSecureUrl()
	{
		return $this->secureUrl;
	}

	/**
	 * Set work
	 *
	 * @param \AppBundle\Entity\Work $work
	 *
	 * @return Image
	 */
	public function setWork(\AppBundle\Entity\Work $work = null)
	{
		$this->work = $work;

		return $this;
	}

	/**
	 * Get work
	 *
	 * @return \AppBundle\Entity\Work
	 */
	public function getWork()
	{
		return $this->work;
	}

	/**
	 * @return mixed
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param mixed $image
	 */
	public function setImage($image)
	{
		$this->image = $image;
	}


}
