<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\User;
use AppBundle\Form\Type\UserType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserApiController extends Controller
{
	/**
	 * @Rest\Get("/users")
	 * @Rest\View(serializerGroups={"user"})
	 */
	public function getUsersAction(  ) {
		$users = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:User')
			->findAll();
		return $users;
	}

	/**
	 * @Rest\Get("/users/{id}")
	 * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"user"})
	 */

	public function getUserAction( Request $request ) {
		$user = $this
			->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:User')
			->find($request->get('id'));
		if (empty($user)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}
		return $user;
	}

	/**
	 * @Rest\Post("/users")
	 * @Rest\View(serializerGroups={"user"})
	 */
	public function postUserAction( Request $request ) {
		$user = new User();
		$form = $this->createForm(UserType::class, $user);
		$form->submit($request->request->all());

		if ($form->isValid()) {
			$encoder = $this->get('security.password_encoder');
			$encoded = $encoder->encodePassword($user, $user->getPlainPassword());
			$user->setPassword($encoded);

			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($user);
			$em->flush();
			return $user;
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\Put("/users/{id}")
	 */
	public function updateUserAction( Request $request ) {
		return $this->updateUser($request, true);
	}

	/**
	 * @Rest\Patch("/users/{id}")
	 */
	public function patchUserAction( Request $request ) {
		return $this->updateUser($request, false);
	}

	private function updateUser( Request $request, $clearMissing ) {
		$user = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:User')
			->find($request->get('id'));

		if (empty($user)) {
			return $this->userNotFound();
		}

		if ($clearMissing) {
			$options = ['validation_groups' => ['Default', 'FullUpdate']];
		} else {
			$options = [];
		}

		$form = $this->createForm(UserType::class, $user, $options);
		$form->submit($request->request->all(), $clearMissing);

		if ($form->isValid()) {
			if (!empty($user->getPlainPassword())) {
				$encoder = $this->get('security.password_encoder');
				$encoded = $encoder->encodePassword($user, $user->getPlainPassword());
				$user->setPassword($encoded);
			}
			$em = $this->get('doctrine.orm.entity_manager');
			$em->merge($user);
			$em->flush();
			return $user;
		} else {
			return $form;
		}
	}

	private function userNotFound() {
		throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User not found');
	}

}
