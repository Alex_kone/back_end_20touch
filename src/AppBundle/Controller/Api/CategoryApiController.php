<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Category;
use AppBundle\Form\Type\CategoryType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CategoryApiController extends Controller {
	/**
	 * @Rest\Get("/categories")
	 * @Rest\View(serializerGroups={"category"})
	 */
	public function getCategoriesAction(  ) {
		$categories = $this->get('doctrine.orm.entity_manager')
		              ->getRepository('AppBundle:Category')
		              ->findAll();
		return $categories;
	}

	/**
	 * @Rest\Get("/categories/{id}")
	 * @Rest\View(serializerGroups={"category"})
	 */
	public function getCategoryAction( Request $request ) {
		$category = $this->get('doctrine.orm.entity_manager')
		                   ->getRepository('AppBundle:Category')
		                   ->find($request->get('id'));

		if (empty($category)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Category not found');
		}
		return $category;
	}

	/**
	 * @Rest\Post("/categories")
	 * @Rest\View()
	 */
	public function postCategoryAction( Request $request ) {
		$category = new Category();
		$form = $this->createForm(CategoryType::class, $category);
		$form->submit($request->request->all());

		if ($form->isValid()) {
			dump($form);
			die();
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\Delete("/categories/{id}")
	 * @Rest\View()
	 */
	public function removeCategoryAction ( Request $request ) {
		$em = $this->get('doctrine.orm.entity_manager');
		$category = $em->getRepository('AppBundle:Category')
		               ->find('id');

		if (empty($category)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Category not found');
		}

		$em->remove($category);
		$em->flush();
	}
}
