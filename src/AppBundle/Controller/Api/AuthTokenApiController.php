<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\AuthToken;
use AppBundle\Entity\Credentials;
use AppBundle\Form\Type\CredentialsType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthTokenApiController extends Controller
{
	/**
	 * @Rest\Post("/auth-tokens")
	 * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"auth-token"})
	 */
	public function postAuthTokenAction( Request $request ) {
		$credentials = new Credentials();
		$form        = $this->createForm( CredentialsType::class, $credentials );
		$form->submit( $request->request->all() );
		if ( ! $form->isValid() ) {
			return $form;
		}

		$em   = $this->get( 'doctrine.orm.entity_manager' );
		$user = $em->getRepository( 'AppBundle:User' )
		           ->findOneBy( [ 'email' => $credentials->getLogin() ] );

		if ( ! $user ) { // L'utilisateur n'existe pas
			return $this->invalidCredentials();
		}

		$encoder         = $this->get( 'security.password_encoder' );
		$isPasswordValid = $encoder->isPasswordValid( $user, $credentials->getPassword() );

		if ( ! $isPasswordValid ) { // Le mot de passe n'est pas correct
			return $this->invalidCredentials();
		}

		$users = $em->getRepository( 'AppBundle:AuthToken' )->findBy( [ 'user' => $user->getId() ] );
		if ( empty( $users ) ) {
			$authToken = new AuthToken();

			$authToken->setValue(base64_encode(random_bytes(50)));
			$authToken->setCreatedAt(new \DateTime('now'));
			$authToken->setUser($user);

			$em->persist($authToken);
			$em->flush();

			return $authToken;
		}

		foreach ( $users as $userAuth ) {
			$userAuth->setValue( base64_encode( random_bytes( 50 ) ) );
			$userAuth->setCreatedAt( new \DateTime( 'now' ) );
			$userAuth->setUser( $user );

			$em->persist( $userAuth );
			$em->flush();

			return $userAuth;
		}

	}


	/**
	 * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
	 * @Rest\Delete("/auth-tokens/{id}")
	 */
	public function removeAuthTokenAction(Request $request)
	{
		$em = $this->get('doctrine.orm.entity_manager');
		$authToken = $em->getRepository('AppBundle:AuthToken')
		                ->find($request->get('id'));
		/* @var $authToken AuthToken */

		$connectedUser = $this->get('security.token_storage')->getToken()->getUser();

		if ($authToken && $authToken->getUser()->getId() === $connectedUser->getId()) {
			$em->remove($authToken);
			$em->flush();
		} else {
			throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
		}
	}

	private function invalidCredentials() {
		return View::create(['message' => 'Invalid credentials'], Response::HTTP_BAD_REQUEST);
	}
}
