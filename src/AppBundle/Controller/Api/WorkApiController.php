<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Work;
use AppBundle\Form\Type\WorkType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WorkApiController extends Controller
{
	/**
	 * @Rest\Get("/works")
	 * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"work"})
	 */
	public function getWorksAction(  ) {
		$works = $this
			->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Work')
			->findAll();

		return $works;
	}

	/**
	 * @Rest\Get("/works/{id}")
	 * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"work"})
	 */
	public function getWorkAction( Request $request ) {
		$work = $this
			->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Work')
			->find($request->get('id'));
		if (empty($work)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}
		return $work;
	}

	/**
	 * @Rest\Post("/works")
	 * @Rest\View()
	 */
	public function postWorkAction( Request $request ) {
		$work = new Work();
		$form = $this->createForm(WorkType::class, $work);
		$form->submit($request->request->all());

		if ($form->isValid()) {
			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($work);
			$em->flush();
			return $work;
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\Delete("/works/{id}")
	 * @Rest\View()
	 */
	public function removeWorkAction( Request $request ) {
		$em = $this->get('doctrine.orm.entity_manager');
		$work = $em->getRepository('AppBundle:Work')
		           ->find($request->get('id'));

		if (empty($work)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}

		$em->remove($work);
		$em->flush();
	}

}
