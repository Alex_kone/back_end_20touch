<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Category;
use AppBundle\Entity\Image;
use AppBundle\Entity\Work;
use AppBundle\Form\Type\ImageType;
use AppBundle\Service\Cloudinary\CloudinaryService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WorksCategoriesApiController extends Controller
{
	/**
	 * @Rest\Get("/works/{id}/categories")
	 * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"work"})
	 */
	public function getCategoriesByWorkAction( Request $request ) {
		$work = $this
			->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Work')
			->find($request->get('id'));

		if (empty($work)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}

		return $work->getCategories();
	}

	/**
	 * @Rest\Get("/works/{id}/images")
	 * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"image"})
	 */
	public function getImagesByWorkAction( Request $request ) {
		$work = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Work')
			->find($request->get('id'));

		if (empty($work)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}

		return $work->getImages();
	}

	/**
	 * @Rest\Post("/works/{id}/categories")
	 * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"work"})
	 */
	public function postCategoryByWorkAction( Request $request ) {
		$em = $this->get('doctrine.orm.entity_manager');
		$work = $em
			->getRepository('AppBundle:Work')
			->find($request->get('id'));

		if (empty($work)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}

		$category = new Category();
		$form = $this->createForm('AppBundle\Form\CategoryType', $category);
		$form->submit($request->request->all());

		if($form->isValid()) {
			$work->addCategory($category);
			$category->addWork($work);

			$em->persist($work);
			$em->persist($category);
			$em->flush();
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\Post("/works/{id}/images")
	 * @Rest\View(serializerGroups={"image"})
	 */
	public function postImageByWorkAction( Request $request, CloudinaryService $cloudService ) {
		$em = $this->get('doctrine.orm.entity_manager');
		$work = $em->getRepository('AppBundle:Work')
			->find($request->get('id'));

		if (empty($work)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}

		$image = new Image();
		$form = $this->createForm(ImageType::class, $image);
		$form->submit($request->files->all());

		if ($form->isValid()) {
			$upload = $cloudService->updateFile($form->getData()->getImage());
			$image
				->setUrl($upload['url'])
				->setSecureUrl($upload['secure_url'])
				->setPublicId($upload['public_id']);
			$image->setWork($work);
			$work->addImage($image);
			$em->persist($image);
			$em->flush();
			return $image;
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\Get("/categories/{id}/works")
	 * @Rest\View(serializerGroups={"category"})
	 */
	public function getWorksByCategoryAction( Request $request ) {
		$category = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Category')
			->find($request->get('id'));

		if (empty($category)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}

		return $category->getWorks();
	}

	/**
	 * @Rest\Post("/categories/{id}/works")
	 * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"work"})
	 */
	public function postWorkByCategoryAction( Request $request ) {
		$em = $this->get('doctrine.orm.entity_manager');
		$category = $em->getRepository('AppBundle:Category')
			->find($request->get('id'));

		if (empty($category)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Work not found');
		}
		$work = new Work();
		$form = $this->createForm('AppBundle\Form\Type\WorkType', $work);
		$form->submit($request->request->all());

		if($form->isValid()) {
			$category->addWork($work);
			$work->addCategory($category);

			$em->persist($work);
			$em->persist($category);
			$em->flush();
		} else {
			return $form;
		}
		return $work;
	}
}

