<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Image;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageApiController extends Controller {

	/**
	 * @Rest\Get("/images")
	 * @Rest\View()
	 */
	public function getImagesAction(  ) {
		$images = $this->get('doctrine.orm.entity_manager')
		               ->getRepository('AppBundle:Image')
		               ->findAll();
		return $images;
	}

	/**
	 * @Rest\Get("/images/{id}")
	 * @Rest\View()
	 */
	public function getImageAction( Request $request ) {
		$image = $this->get('doctrine.orm.entity_manager')
		               ->getRepository('AppBundle:Image')
		               ->findAll();

		if (empty($image)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Image not found');
		}

		return $image;
	}

}
