<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Contact;
use AppBundle\Form\Type\ContactType;
use AppBundle\Service\Mailer\AppMailer;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactApiController extends Controller
{
	/**
	 * Show all Contacts
	 * @Rest\Get("/contacts")
	 * @Rest\View()
	 */
	public function getContactsAction(  ) {
		$contacts = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Contact')
			->findAll();

		return $contacts;
	}

	/**
	 * Show contact by ID
	 * @Rest\Get("/contacts/{id}")
	 * @Rest\View()
	 */
	public function getContactAction( Request $request ) {
		$contact = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Contact')
			->find('id');

		if (empty($contact)) {
			return new JsonResponse(['message' => 'Contact not found'], Response::HTTP_NOT_FOUND);
		}

		return $contact;
	}

	/**
	 * Create new contact
	 * @Rest\Post("/contacts")
	 * @Rest\View()
	 */
	public function postContactAction( Request $request, AppMailer $mailer ) {
		$contact = new Contact();
		$form = $this->createForm(ContactType::class, $contact);
		$form->submit($request->request->all());

		if ($form->isValid()) {

			$mailer->sendMailNotification($contact);

			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($contact);
			$em->flush();


			return $contact;
		} else {
			return $form;
		}
	}

	/**
	 * Delete contact by ID
	 * @Rest\Delete("/contacts/{id}")
	 * @Rest\View()
	 */
	public function removeContactAction( Request $request ) {
		$em = $this->get('doctrine.orm.entity_manager');
		$contact = $em->getRepository('AppBundle:Contact')
			->find($request->get('id'));

		if (empty($contact)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Contact not found');
		}

		$em->remove($contact);
		$em->flush();
	}

	/**
	 * @Rest\View()
	 * @Rest\Put("/contacts/{id}")
	 */
	public function updateContactAction(Request $request)
	{
		return $this->updateContact($request, true);
	}

	/**
	 * @Rest\View()
	 * @Rest\Patch("/contacts/{id}")
	 */
	public function patchContactAction(Request $request)
	{
		return $this->updateContact($request, false);
	}

	private function updateContact(Request $request, $clearMissing)
	{
		$contact = $this->get('doctrine.orm.entity_manager')
		              ->getRepository('AppBundle:Contact')
		              ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
		/* @var $contact Contact */

		if (empty($contact)) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Contact not found');
		}

		$form = $this->createForm(ContactType::class, $contact);

		// Le paramètre false dit à Symfony de garder les valeurs dans notre
		// entité si l'utilisateur n'en fournit pas une dans sa requête
		$form->submit($request->request->all(), $clearMissing);

		if ($form->isValid()) {
			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($contact);
			$em->flush();
			return $contact;
		} else {
			return $form;
		}
	}
}
