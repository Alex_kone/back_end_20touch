<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkType extends AbstractType
{
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name')
			->add('description')
			->add('content')
			->add('slug')
//			->add('isActive')
//			->add('images', EntityType::class, [
//				'class' => 'AppBundle\Entity\Image',
//				'choice_label' => 'id',
//				'multiple' => true
//			])
//			->add('categories', EntityType::class, [
//				'class' => 'AppBundle\Entity\Category',
//				'choice_label' => 'name',
//				'multiple' => true
//			])
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\Work',
			'csrf_protection' => false
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix()
	{
		return 'appbundle_work';
	}


}
