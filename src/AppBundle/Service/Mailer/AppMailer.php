<?php

namespace AppBundle\Service\Mailer;

use AppBundle\Entity\Contact;
use AppBundle\Service\Mailer;
use Symfony\Component\Templating\EngineInterface;

class AppMailer {

	/**
	 * @var \Swift_Mailer
	 */
	private $mailer;

	/**
	 * @var EngineInterface
	 */
	private $template;

	public function __construct( \Swift_Mailer $mailer, EngineInterface $template ) {

		$this->mailer = $mailer;
		$this->template = $template;
	}

	public function sendMailNotification( Contact $contact ) {
			$message = \Swift_Message::newInstance()
					->setFrom($contact->getEmail())
					->setTo('recipient@exemple.com')
					->setBody($this->template->render(':Mail:notification.html.twig',[
						'contact' => $contact
					]));

			return $this->mailer->send($message);
	}
}